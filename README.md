MfGames.Markdown.Gemtext CIL
============================

An extension for [Markdig](https://github.com/xoofx/markdig) that converts Markdown into Gemtext.
