## [1.2.1](https://gitlab.com/mfgames-cil/mfgames-markdown-gemtext-cil/compare/v1.2.0...v1.2.1) (2022-02-16)


### Bug Fixes

* extra newline after tables ([f53405b](https://gitlab.com/mfgames-cil/mfgames-markdown-gemtext-cil/commit/f53405b55eb29f4288a72ffb7314394a1f19cd25))

# [1.2.0](https://gitlab.com/mfgames-cil/mfgames-markdown-gemtext-cil/compare/v1.1.0...v1.2.0) (2022-02-16)


### Features

* added the abilty to add or remove the preformat block around tables ([0649c62](https://gitlab.com/mfgames-cil/mfgames-markdown-gemtext-cil/commit/0649c62685f16258daeba9450e0a09bcbe1352c0))

# [1.1.0](https://gitlab.com/mfgames-cil/mfgames-markdown-gemtext-cil/compare/v1.0.0...v1.1.0) (2022-02-16)


### Features

* added table rendering ([6fac646](https://gitlab.com/mfgames-cil/mfgames-markdown-gemtext-cil/commit/6fac646f189fdcc061c868b99e4f1174c08033ec))

# 1.0.0 (2021-09-07)


### Bug Fixes

* **test:** adding coverage test ([9250289](https://gitlab.com/mfgames-cil/mfgames-markdown-gemtext-cil/commit/925028984d782fc8f7a9a90812a6a57ae90cc617))
* correcting references and NuGet metadata ([de17b4a](https://gitlab.com/mfgames-cil/mfgames-markdown-gemtext-cil/commit/de17b4a75b3eed733f36032365d0f21d6a02e955))


### Features

* initial commit ([9fe90f8](https://gitlab.com/mfgames-cil/mfgames-markdown-gemtext-cil/commit/9fe90f820c7333d5cbc9b74cb301e64849ec21ca))
