using MfGames.Markdown.Gemtext;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    public class ListTests
    {
        [Fact(Skip = "Not ready to figure this out.")]
        public void NestedLists()
        {
            string input = string.Join(
                "\n",
                "- A",
                "    - B",
                "- C");
            string expected = string.Join(
                "\n",
                "* A",
                "* B",
                "* C");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
