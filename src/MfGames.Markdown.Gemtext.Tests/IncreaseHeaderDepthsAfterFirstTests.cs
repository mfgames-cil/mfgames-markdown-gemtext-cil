﻿using Markdig;
using MfGames.Markdown.Gemtext;
using MfGames.Markdown.Gemtext.Extensions;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    public class IncreaseHeaderDepthsAfterFirstTests
    {
        private readonly string one1s;

        private readonly string two1s;

        public IncreaseHeaderDepthsAfterFirstTests()
        {
            this.two1s = string.Join(
                "\n",
                "# Heading 1",
                "",
                "Line",
                "",
                "# Heading 2");
            this.one1s = string.Join(
                "\n",
                "# Heading 1",
                "",
                "Line",
                "",
                "## Heading 2");
        }

        [Fact]
        public void WithOne1s()
        {
            string expected = string.Join(
                "\n",
                "# Heading 1",
                "",
                "Line",
                "",
                "## Heading 2");
            string actual = MarkdownGemtext.ToGemtext(
                this.one1s,
                new MarkdownPipelineBuilder()
                    .Use<IncreaseHeaderDepthsAfterFirst>()
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WithoutOne1s()
        {
            string expected = string.Join(
                "\n",
                "# Heading 1",
                "",
                "Line",
                "",
                "## Heading 2");
            string actual = MarkdownGemtext.ToGemtext(
                this.one1s,
                new MarkdownPipelineBuilder()
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WithoutTwo1s()
        {
            string expected = string.Join(
                "\n",
                "# Heading 1",
                "",
                "Line",
                "",
                "# Heading 2");
            string actual = MarkdownGemtext.ToGemtext(
                this.two1s,
                new MarkdownPipelineBuilder()
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WithTwo1s()
        {
            string expected = string.Join(
                "\n",
                "# Heading 1",
                "",
                "Line",
                "",
                "## Heading 2");
            string actual = MarkdownGemtext.ToGemtext(
                this.two1s,
                new MarkdownPipelineBuilder()
                    .Use<IncreaseHeaderDepthsAfterFirst>()
                    .Build());

            Assert.Equal(expected, actual);
        }
    }
}
