using Markdig;
using MfGames.Markdown.Gemtext;
using MfGames.Markdown.Gemtext.Extensions;
using MfGames.Markdown.Gemtext.Renderers;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    /// <summary>
    /// Tests the various functionality of the ParagraphLinkHandling and
    /// EndLinkInlineFormatting logic works.
    /// </summary>
    public class ParagraphLinkHandlingTests
    {
        private readonly string input;

        public ParagraphLinkHandlingTests()
        {
            this.input = string.Join(
                "\n",
                "This is a paragraph with an [inline](https://example.com) link.",
                "Here is [another](https://example.org/) link, part of the same paragraph.",
                "",
                "This is a second paragraph, with a different [link](https://duck.com) in it.");
        }

        [Fact]
        public void DefaultHandling()
        {
            string expected = string.Join(
                "\n",
                "This is a paragraph with an",
                "=> https://example.com inline",
                "link. Here is",
                "=> https://example.org/ another",
                "link, part of the same paragraph.",
                "",
                "This is a second paragraph, with a different",
                "=> https://duck.com link",
                "in it.");
            string actual = MarkdownGemtext.ToGemtext(
                this.input,
                new MarkdownPipelineBuilder()
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DocumentEndHandling()
        {
            string expected = string.Join(
                "\n",
                "This is a paragraph with an inline[1] link. Here is another[2] link, part of the same paragraph.",
                "",
                "This is a second paragraph, with a different link[3] in it.",
                "",
                "=> https://example.com 1: https://example.com",
                "=> https://example.org/ 2: https://example.org/",
                "=> https://duck.com 3: https://duck.com");
            string actual = MarkdownGemtext.ToGemtext(
                this.input,
                new MarkdownPipelineBuilder()
                    .Use(
                        new SetBlockLinkHandling(
                            BlockLinkHandling.DocumentEnd))
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ParagraphEndHandling()
        {
            string expected = string.Join(
                "\n",
                "This is a paragraph with an inline[1] link. Here is another[2] link, part of the same paragraph.",
                "",
                "=> https://example.com 1: https://example.com",
                "=> https://example.org/ 2: https://example.org/",
                "",
                "This is a second paragraph, with a different link[3] in it.",
                "",
                "=> https://duck.com 3: https://duck.com");
            string actual = MarkdownGemtext.ToGemtext(
                this.input,
                new MarkdownPipelineBuilder()
                    .Use(
                        new SetBlockLinkHandling(
                            BlockLinkHandling.ParagraphEnd))
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ParagraphEndHandlingWithFootnote()
        {
            string expected = string.Join(
                "\n",
                "This is a paragraph with an inline[10] link. Here is another[11] link, part of the same paragraph.",
                "",
                "=> https://example.com 10: https://example.com",
                "=> https://example.org/ 11: https://example.org/",
                "",
                "This is a second paragraph, with a different link[12] in it.",
                "",
                "=> https://duck.com 12: https://duck.com");
            string actual = MarkdownGemtext.ToGemtext(
                this.input,
                new MarkdownPipelineBuilder()
                    .Use(
                        new SetBlockLinkHandling(
                            BlockLinkHandling.ParagraphEnd,
                            nextFootnoteNumber: 10))
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ParagraphEndHandlingWithTextLinks()
        {
            string expected = string.Join(
                "\n",
                "This is a paragraph with an inline link. Here is another link, part of the same paragraph.",
                "",
                "=> https://example.com inline",
                "=> https://example.org/ another",
                "",
                "This is a second paragraph, with a different link in it.",
                "",
                "=> https://duck.com link");
            string actual = MarkdownGemtext.ToGemtext(
                this.input,
                new MarkdownPipelineBuilder()
                    .Use(
                        new SetBlockLinkHandling(
                            BlockLinkHandling.ParagraphEnd,
                            EndLinkInlineFormatting.Text))
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveHandling()
        {
            string expected = string.Join(
                "\n",
                "This is a paragraph with an inline link. "
                + "Here is another link, part of the same paragraph.",
                "",
                "This is a second paragraph, with a different link in it.");
            string actual = MarkdownGemtext.ToGemtext(
                this.input,
                new MarkdownPipelineBuilder()
                    .Use(new SetBlockLinkHandling(BlockLinkHandling.Remove))
                    .Build());

            Assert.Equal(expected, actual);
        }
    }
}
