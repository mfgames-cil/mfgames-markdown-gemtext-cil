using Markdig;
using MfGames.Markdown.Gemtext;
using MfGames.Markdown.Gemtext.Extensions;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests.PythonInspired
{
    /// <summary>
    /// Runs through various plain input tests.
    /// Based on
    /// https://github.com/makeworld-the-better-one/md2gemini/blob/master/tests/test_plain.py
    /// </summary>
    public class PlainTextPythonTests
    {
        [Fact]
        public void ConvertHtmlBlock()
        {
            string input = string.Join(
                "\n",
                "<div>",
                "<tag>html is here</tag>",
                "</div>",
                "",
                "Text after");
            string expected = string.Join(
                "\n",
                "```html",
                "<div>",
                "<tag>html is here</tag>",
                "</div>",
                "```",
                "",
                "Text after");
            string actual = MarkdownGemtext.ToGemtext(
                input,
                new MarkdownPipelineBuilder()
                    .Use<HtmlAsCodeBlocks>()
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EmptyInput()
        {
            string input = "";
            string expected = "";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveBold()
        {
            string input = "Sentence with **bold** in it.";
            string expected = "Sentence with bold in it.";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveBold2()
        {
            string input = "Sentence with __bold__ in it.";
            string expected = "Sentence with bold in it.";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveInlineHtml()
        {
            string input =
                "<i>test1</i> <b>test2</b> <em>test3</em> <made-up>test4</made-up>";
            string expected = "test1 test2 test3 test4";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveItalics()
        {
            string input = "Sentence with *italics* in it.";
            string expected = "Sentence with italics in it.";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveItalics2()
        {
            string input = "Sentence with _italics_ in it.";
            string expected = "Sentence with italics in it.";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
