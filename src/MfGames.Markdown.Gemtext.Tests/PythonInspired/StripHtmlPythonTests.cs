using Markdig;

using MfGames.Markdown.Gemtext;
using MfGames.Markdown.Gemtext.Extensions;
using MfGames.Markdown.Gemtext.Renderers;

using Xunit;

namespace MfGames.Markdown.Gemini.Tests.PythonInspired
{
    /// <summary>
    /// https://github.com/makeworld-the-better-one/md2gemini/blob/master/tests/test_strip_html.py
    /// </summary>
    public class StripHtmlPythonTests
    {
        [Fact]
        public void KeepBold()
        {
            string input = "Sentence with **bold** in it.";
            string expected = "Sentence with **bold** in it.";
            var inlineFormatting = new SetInlineFormatting()
            {
                Default = InlineFormatting.Normalize,
            };
            string actual = MarkdownGemtext.ToGemtext(
                input,
                new MarkdownPipelineBuilder()
                    .Use(inlineFormatting)
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void KeepBold2()
        {
            string input = "Sentence with __bold__ in it.";
            string expected = "Sentence with **bold** in it.";
            var inlineFormatting = new SetInlineFormatting()
            {
                Default = InlineFormatting.Normalize,
            };
            string actual = MarkdownGemtext.ToGemtext(
                input,
                new MarkdownPipelineBuilder()
                    .Use(inlineFormatting)
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void KeepItalics()
        {
            string input = "Sentence with *italics* in it.";
            string expected = "Sentence with *italics* in it.";
            var inlineFormatting = new SetInlineFormatting()
            {
                Default = InlineFormatting.Normalize,
            };
            string actual = MarkdownGemtext.ToGemtext(
                input,
                new MarkdownPipelineBuilder()
                    .Use(inlineFormatting)
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void KeepItalics2()
        {
            string input = "Sentence with _italics_ in it.";
            string expected = "Sentence with *italics* in it.";
            var inlineFormatting = new SetInlineFormatting()
            {
                Default = InlineFormatting.Normalize,
            };
            string actual = MarkdownGemtext.ToGemtext(
                input,
                new MarkdownPipelineBuilder()
                    .Use(inlineFormatting)
                    .Build());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveHtmlBlock()
        {
            string input =
                "<div>\n<tag>html is here</tag>\n</div>\n\nText after";
            string expected = "Text after";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RemoveInlineHtml()
        {
            string input =
                "<i>test1</i> <b>test2</b> <em>test3</em> <made-up>test4</made-up>";
            string expected = "test1 test2 test3 test4";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
