using MfGames.Markdown.Gemtext;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests.PythonInspired
{
    /// <summary>
    /// https://github.com/makeworld-the-better-one/md2gemini/blob/master/tests/test_codeblock.py
    /// </summary>
    public class CodeBlockPythonTests
    {
        [Fact]
        public void EndsWithNoNewLines()
        {
            string input = string.Join(
                "\n",
                "Non code block",
                "",
                "    code block here");
            string expected = string.Join(
                "\n",
                "Non code block",
                "",
                "```",
                "code block here",
                "```");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NoExtraNewline()
        {
            string input = string.Join(
                "\n",
                "Non code block",
                "",
                "    code block here",
                "",
                "More non code");
            string expected = string.Join(
                "\n",
                "Non code block",
                "",
                "```",
                "code block here",
                "```",
                "",
                "More non code");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact(Skip = "Not sure how to implement or justification of this one")]
        public void WithExtraNewline()
        {
            string input = string.Join(
                "\n",
                "Non code block",
                "",
                "    code block here",
                "",
                "",
                "More non code");
            string expected = string.Join(
                "\n",
                "Non code block",
                "",
                "```",
                "code block here",
                "",
                "```",
                "",
                "More non code");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
