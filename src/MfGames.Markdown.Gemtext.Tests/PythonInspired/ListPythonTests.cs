using MfGames.Markdown.Gemtext;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests.PythonInspired
{
    /// <summary>
    /// https://github.com/makeworld-the-better-one/md2gemini/blob/master/tests/test_list.py
    /// </summary>
    public class ListPythonTests
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        public void ListWithNewlineAndSpaces(int request)
        {
            string padding = new string(' ', request);
            string input = $"- hello\n{padding}world\n- oh";
            string expected = "* hello world\n* oh";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NormalList()
        {
            string input = "- hi\n- oh";
            string expected = "* hi\n* oh";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SeparateLists()
        {
            string input = "* hello\n\n- oh";
            string expected = "* hello\n\n* oh";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
