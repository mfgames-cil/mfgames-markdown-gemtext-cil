using MfGames.Markdown.Gemtext;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests.PythonInspired
{
    /// <summary>
    /// https://github.com/makeworld-the-better-one/md2gemini/blob/master/tests/test_base_url.py
    /// </summary>
    public class BaseUrlPythonTests
    {
        [Fact]
        public void AbsoluteUrl()
        {
            string input = "[test](https://duck.com/test)";
            string expected = "=> https://duck.com/test test";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RootUrl()
        {
            string input = "[test](/url)";
            string expected = "=> /url test";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
