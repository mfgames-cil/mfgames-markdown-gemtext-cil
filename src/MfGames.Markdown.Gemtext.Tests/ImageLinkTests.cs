using MfGames.Markdown.Gemtext;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    public class ImageLinkTests
    {
        [Fact]
        public void NamedImageLink()
        {
            string input = "![test](image.jpg)";
            string expected = "=> image.jpg test";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SimpleImageLink()
        {
            string input = "![](image.jpg)";
            string expected = "=> image.jpg";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
