using MfGames.Markdown.Gemtext;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    public class HeaderTests
    {
        [Fact]
        public void BetweenParagraphs()
        {
            string input = string.Join(
                "\n",
                "Paragraph One",
                "",
                "# Header 1",
                "",
                "Paragraph Two");
            string expected = string.Join(
                "\n",
                "Paragraph One",
                "",
                "# Header 1",
                "",
                "Paragraph Two");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void FollowedByText()
        {
            string input = string.Join(
                "\n",
                "# Header 1",
                "Text Block");
            string expected = string.Join(
                "\n",
                "# Header 1",
                "",
                "Text Block");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void H1()
        {
            string input = "# Header 1";
            string expected = "# Header 1";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void H2()
        {
            string input = "## Header 2";
            string expected = "## Header 2";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void H3()
        {
            string input = "### Header 3";
            string expected = "### Header 3";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void H4()
        {
            string input = "#### Header 4";
            string expected = "Header 4";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void H5()
        {
            string input = "##### Header 5";
            string expected = "Header 5";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void H6()
        {
            string input = "###### Header 6";
            string expected = "Header 6";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
