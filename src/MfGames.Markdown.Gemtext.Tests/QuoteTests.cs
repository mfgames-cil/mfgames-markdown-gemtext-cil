using Markdig;
using Markdig.Extensions.SmartyPants;
using MfGames.Markdown.Gemtext;
using MfGames.Markdown.Gemtext.Extensions;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    public class QuoteTests
    {
        [Fact]
        public void NormalDouble()
        {
            string input = "Normal \"double\" quote.";
            string expected = "Normal \"double\" quote.";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SmartyDouble()
        {
            GemtextSmartyPantsExtension smartyPants =
                new(new SmartyPantOptions());
            string input = "Normal \"double\" quote.";
            string expected = "Normal “double” quote.";
            string actual = MarkdownGemtext.ToGemtext(
                input,
                new MarkdownPipelineBuilder()
                    .Use(smartyPants)
                    .Build());

            Assert.Equal(expected, actual);
        }
    }
}
