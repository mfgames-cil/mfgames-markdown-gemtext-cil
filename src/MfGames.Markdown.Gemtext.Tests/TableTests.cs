using ConsoleTableExt;

using Markdig;

using MfGames.Markdown.Gemtext;
using MfGames.Markdown.Gemtext.Extensions;

using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    public class TableTests
    {
        [Fact]
        public void AlignedTable()
        {
            MarkdownPipeline pipeline = new MarkdownPipelineBuilder()
                .Use(
                    new GemtextPipeTableExtension(
                        new GemtextPipeTableOptions()
                        {
                            OmitPreformatLines = true,
                            ConfigureTableBuilder = (x) =>
                                x.WithFormat(
                                    ConsoleTableBuilderFormat.MarkDown),
                        }))
                .Build();
            string input = string.Join(
                "\n",
                "aaa|bbb|ccc",
                "--:|---|:-:",
                "1|2|3",
                "4|5|6");
            string expected = string.Join(
                "\n",
                "| aaa | bbb | ccc |",
                "|-----|-----|-----|",
                "|   1 | 2   |  3  |",
                "|   4 | 5   |  6  |",
                "");
            string actual = MarkdownGemtext.ToGemtext(input, pipeline);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void FencedTable()
        {
            MarkdownPipeline pipeline = new MarkdownPipelineBuilder()
                .Use(
                    new GemtextPipeTableExtension(
                        new GemtextPipeTableOptions()
                        {
                            ConfigureTableBuilder = (x) =>
                                x.WithFormat(
                                    ConsoleTableBuilderFormat.MarkDown),
                        }))
                .Build();
            string input = string.Join(
                "\n",
                "aaa|bbb|ccc",
                "--:|---|:-:",
                "1|2|3",
                "4|5|6");
            string expected = string.Join(
                "\n",
                "```",
                "| aaa | bbb | ccc |",
                "|-----|-----|-----|",
                "|   1 | 2   |  3  |",
                "|   4 | 5   |  6  |",
                "```",
                "",
                "");
            string actual = MarkdownGemtext.ToGemtext(input, pipeline);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SimpleTable()
        {
            MarkdownPipeline pipeline = new MarkdownPipelineBuilder()
                .Use(
                    new GemtextPipeTableExtension(
                        new GemtextPipeTableOptions()
                        {
                            OmitPreformatLines = true,
                            ConfigureTableBuilder = (x) =>
                                x.WithFormat(
                                    ConsoleTableBuilderFormat.MarkDown),
                        }))
                .Build();
            string input = string.Join(
                "\n",
                "a|b|c",
                "-|-|",
                "1|2|3",
                "4|5|6");
            string expected = string.Join(
                "\n",
                "| a | b | c |",
                "|---|---|---|",
                "| 1 | 2 | 3 |",
                "| 4 | 5 | 6 |",
                "");
            string actual = MarkdownGemtext.ToGemtext(input, pipeline);

            Assert.Equal(expected, actual);
        }
    }
}
