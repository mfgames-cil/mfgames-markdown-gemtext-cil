using MfGames.Markdown.Gemtext;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    public class LinkTests
    {
        [Fact]
        public void BareLink()
        {
            string input = "[](/url)";
            string expected = "=> /url";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ListOfLinks()
        {
            string input = string.Join(
                "\n",
                "- [](/a)",
                "- [Two](/b)",
                "- [Three](/c/d)");
            string expected = string.Join(
                "\n",
                "=> /a",
                "=> /b Two",
                "=> /c/d Three");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MixedLinkOfLists()
        {
            string input = string.Join(
                "\n",
                "- [](/a)",
                "- Two",
                "- [Three](/c/d)");
            string expected = string.Join(
                "\n",
                "=> /a",
                "* Two",
                "=> /c/d Three");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PlainLink()
        {
            string input = "[test](/url)";
            string expected = "=> /url test";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
