using Markdig;
using MfGames.Markdown.Gemtext;
using MfGames.Markdown.Gemtext.Extensions;
using MfGames.Markdown.Gemtext.Renderers;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    public class CodeInlineTests
    {
        [Fact]
        public void Default()
        {
            string input = "Normal `code` quote.";
            string expected = "Normal code quote.";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Normalized()
        {
            string input = "Normal `code` quote.";
            string expected = "Normal `code` quote.";
            string actual = MarkdownGemtext.ToGemtext(
                input,
                new MarkdownPipelineBuilder()
                    .Use(
                        new SetInlineFormatting
                        {
                            Code = InlineFormatting.Normalize,
                        })
                    .Build());

            Assert.Equal(expected, actual);
        }
    }
}
