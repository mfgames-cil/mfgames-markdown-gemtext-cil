using MfGames.Markdown.Gemtext;
using Xunit;

namespace MfGames.Markdown.Gemini.Tests
{
    public class PlainTextTests
    {
        [Fact]
        public void NormalText()
        {
            string input = "This is input.";
            string expected = "This is input.";
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Paragraphs()
        {
            // Okay, this one isn't based on the test_plain.py, but seems
            // useful to have while converting logic.
            string input = string.Join(
                "\n",
                "Paragraph One",
                string.Empty,
                "Paragraph Two");
            string expected = string.Join(
                "\n",
                "Paragraph One",
                string.Empty,
                "Paragraph Two");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SoftBreakParagraphs()
        {
            // Okay, this one isn't based on the test_plain.py, but seems
            // useful to have while converting logic.
            string input = string.Join(
                "\n",
                "Paragraph",
                "One",
                "    Three",
                string.Empty,
                "Paragraph Two");
            string expected = string.Join(
                "\n",
                "Paragraph One Three",
                string.Empty,
                "Paragraph Two");
            string actual = MarkdownGemtext.ToGemtext(input);

            Assert.Equal(expected, actual);
        }
    }
}
