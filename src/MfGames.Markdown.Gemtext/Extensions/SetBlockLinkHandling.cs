using Markdig;
using Markdig.Renderers;
using MfGames.Markdown.Gemtext.Renderers;

namespace MfGames.Markdown.Gemtext.Extensions
{
    /// <summary>
    /// Extension method to control how links are processed inside blocks.
    /// </summary>
    /// <seealso cref="IMarkdownExtension" />
    public class SetBlockLinkHandling : IMarkdownExtension
    {
        public SetBlockLinkHandling(
            BlockLinkHandling? blockLinkHandling = null,
            EndLinkInlineFormatting? endLinkInlineFormatting = null,
            int? nextFootnoteNumber = null)
        {
            this.BlockLinkHandling = blockLinkHandling;
            this.EndLinkInlineFormatting = endLinkInlineFormatting;
            this.NextFootnoteNumber = nextFootnoteNumber;
        }

        /// <summary>
        /// Gets or sets how block links are handled. If this is null, then no
        /// change is made to the current renderer.
        /// </summary>
        public BlockLinkHandling? BlockLinkHandling { get; set; }

        /// <summary>
        /// Gets or sets how links are formatted if they are gathered to the
        /// end of the paragraph or document. If this is null, then no change
        /// will be made.
        /// </summary>
        public EndLinkInlineFormatting? EndLinkInlineFormatting { get; set; }

        /// <summary>
        /// Gets or sets the next footnote number. If this is null, then no
        /// change will be made.
        /// </summary>
        public int? NextFootnoteNumber { get; set; }

        /// <inheritdoc />
        public void Setup(MarkdownPipelineBuilder pipeline)
        {
        }

        /// <inheritdoc />
        public void Setup(MarkdownPipeline pipeline, IMarkdownRenderer renderer)
        {
            if (renderer is not GemtextRenderer gemtext)
            {
                return;
            }

            gemtext.BlockLinkHandling = this.BlockLinkHandling
                ?? gemtext.BlockLinkHandling;
            gemtext.EndLinkInlineFormatting = this.EndLinkInlineFormatting
                ?? gemtext.EndLinkInlineFormatting;
            gemtext.NextFootnoteNumber = this.NextFootnoteNumber
                ?? gemtext.NextFootnoteNumber;
        }
    }
}
