using Markdig;
using Markdig.Extensions.Tables;
using Markdig.Parsers.Inlines;
using Markdig.Renderers;

using MfGames.Markdown.Gemtext.Renderers;
using MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks;

namespace MfGames.Markdown.Gemtext.Extensions
{
    /// <summary>
    /// Extension method to control how links are processed inside blocks.
    /// </summary>
    /// <seealso cref="IMarkdownExtension" />
    public class GemtextPipeTableExtension : IMarkdownExtension
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GemtextPipeTableExtension" />
        /// class.
        /// </summary>
        /// <param name="options">The options.</param>
        public GemtextPipeTableExtension(
            GemtextPipeTableOptions? options = null)
        {
            this.Options = options ?? new GemtextPipeTableOptions();
        }

        /// <summary>
        /// Gets the options.
        /// </summary>
        public GemtextPipeTableOptions Options { get; }

        /// <inheritdoc />
        public void Setup(MarkdownPipelineBuilder pipeline)
        {
            pipeline.PreciseSourceLocation = true;

            if (!pipeline.BlockParsers.Contains<PipeTableBlockParser>())
            {
                pipeline.BlockParsers.Insert(0, new PipeTableBlockParser());
            }

            LineBreakInlineParser? lineBreakParser =
                pipeline.InlineParsers.FindExact<LineBreakInlineParser>();

            if (!pipeline.InlineParsers.Contains<PipeTableParser>())
            {
                pipeline.InlineParsers.InsertBefore<EmphasisInlineParser>(
                    new PipeTableParser(lineBreakParser!, this.Options));
            }
        }

        /// <inheritdoc />
        public void Setup(MarkdownPipeline pipeline, IMarkdownRenderer renderer)
        {
            if (renderer is not GemtextRenderer gemtext)
            {
                return;
            }

            gemtext.ObjectRenderers.Add(
                new TableRenderer(
                    this.Options.OmitPreformatLines,
                    this.Options.ConfigureTableBuilder));
        }
    }
}
