using Markdig;
using Markdig.Renderers;
using MfGames.Markdown.Gemtext.Renderers;
using MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks;

namespace MfGames.Markdown.Gemtext.Extensions
{
    /// <summary>
    /// Extension method to control the depth of the headers in a file so that
    /// the first one (maybe a header) is H1 but the others are decreased to
    /// H2 or lower depending on their initial level.
    /// </summary>
    /// <seealso cref="IMarkdownExtension" />
    public class IncreaseHeaderDepthsAfterFirst : IMarkdownExtension
    {
        /// <inheritdoc />
        public void Setup(MarkdownPipelineBuilder pipeline)
        {
        }

        /// <inheritdoc />
        public void Setup(MarkdownPipeline pipeline, IMarkdownRenderer renderer)
        {
            if (renderer is not GemtextRenderer gemtext)
            {
                return;
            }

            var heading = gemtext.ObjectRenderers.Find<HeadingRenderer>();

            if (heading != null)
            {
                heading.IncreaseHeaderDepthAfterFirst = true;
            }
        }
    }
}
