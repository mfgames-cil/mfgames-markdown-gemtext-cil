using Markdig;
using Markdig.Renderers;
using MfGames.Markdown.Gemtext.Renderers;

namespace MfGames.Markdown.Gemtext.Extensions
{
    /// <summary>
    /// Extension method to turn all inline formatting from the default of
    /// removing to render in normalizing in rendered form.
    /// </summary>
    /// <seealso cref="IMarkdownExtension" />
    public class SetInlineFormatting : IMarkdownExtension
    {
        /// <summary>
        /// Gets or sets the override formatting for code lines
        /// (backtick) spans.
        /// </summary>
        public InlineFormatting? Code { get; set; }

        /// <summary>
        /// Sets or sets the override formatting for all inlines.
        /// </summary>
        public InlineFormatting? Default { get; set; }

        /// <summary>
        /// Gets or sets the override formatting for emphasis (italic
        /// and bold) spans.
        /// </summary>
        public InlineFormatting? Emphasis { get; set; }

        /// <inheritdoc />
        public void Setup(MarkdownPipelineBuilder pipeline)
        {
        }

        /// <inheritdoc />
        public void Setup(MarkdownPipeline pipeline, IMarkdownRenderer renderer)
        {
            if (renderer is not GemtextRenderer gemtext)
            {
                return;
            }

            gemtext.InlineFormatting = this.Default ?? gemtext.InlineFormatting;
            gemtext.EmphasisFormatting = this.Emphasis;
            gemtext.CodeFormatting = this.Code;
        }
    }
}
