using Markdig;
using Markdig.Extensions.SmartyPants;
using Markdig.Parsers.Inlines;
using Markdig.Renderers;
using MfGames.Markdown.Gemtext.Renderers;
using MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines;

namespace MfGames.Markdown.Gemtext.Extensions
{
    /// <summary>
    /// Extension to enable SmartyPants, but for Gemtext.
    /// </summary>
    public class GemtextSmartyPantsExtension : IMarkdownExtension
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SmartyPantsExtension" /> class.
        /// </summary>
        /// <param name="options">The options.</param>
        public GemtextSmartyPantsExtension(SmartyPantOptions? options)
        {
            this.Options = options ?? new SmartyPantOptions();
        }

        /// <summary>
        /// Gets the options.
        /// </summary>
        public SmartyPantOptions Options { get; }

        public void Setup(MarkdownPipelineBuilder pipeline)
        {
            if (!pipeline.InlineParsers.Contains<SmartyPantsInlineParser>())
            {
                // Insert the parser after the code span parser
                pipeline.InlineParsers.InsertAfter<CodeInlineParser>(
                    new SmartyPantsInlineParser());
            }
        }

        public void Setup(MarkdownPipeline pipeline, IMarkdownRenderer renderer)
        {
            if (renderer is not GemtextRenderer gemtextRenderer)
            {
                return;
            }

            if (!gemtextRenderer.ObjectRenderers
                .Contains<GemtextSmartyPantRenderer>())
            {
                gemtextRenderer.ObjectRenderers.Add(
                    new GemtextSmartyPantRenderer(this.Options));
            }
        }
    }
}
