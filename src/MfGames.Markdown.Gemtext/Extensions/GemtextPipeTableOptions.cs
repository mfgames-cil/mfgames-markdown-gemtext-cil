using System;

using ConsoleTableExt;

using Markdig.Extensions.Tables;

namespace MfGames.Markdown.Gemtext.Extensions
{
    public class GemtextPipeTableOptions : PipeTableOptions
    {
        /// <summary>
        /// Gets or sets the table builder to control formatting.
        /// </summary>
        public Action<ConsoleTableBuilder>? ConfigureTableBuilder { get; set; }

        /// <summary>
        /// Gets or sets a value whether the preformat (backticks) fence should
        /// not be emitted.
        /// </summary>
        public bool OmitPreformatLines { get; set; }
    }
}
