using Markdig;
using Markdig.Renderers;
using MfGames.Markdown.Gemtext.Renderers;

namespace MfGames.Markdown.Gemtext.Extensions
{
    /// <summary>
    /// Extension method to retain HTML blocks as a code fenced block with
    /// "html" as the data.
    /// </summary>
    /// <seealso cref="IMarkdownExtension" />
    public class HtmlAsCodeBlocks : IMarkdownExtension
    {
        /// <inheritdoc />
        public void Setup(MarkdownPipelineBuilder pipeline)
        {
        }

        /// <inheritdoc />
        public void Setup(MarkdownPipeline pipeline, IMarkdownRenderer renderer)
        {
            if (renderer is GemtextRenderer gemtext)
            {
                gemtext.HtmlBlockFormatting = HtmlBlockFormatting.CodeBlock;
            }
        }
    }
}
