namespace MfGames.Markdown.Gemtext.Renderers
{
    /// <summary>
    /// Describes the ways of formatting a HTML block.
    /// </summary>
    public enum HtmlBlockFormatting
    {
        /// <summary>
        /// Indicates that HTML code blocks should just be removed.
        /// </summary>
        Remove,

        /// <summary>
        /// Indicates that HTML code blocks should be treated as blocks with
        /// "html" as the type.
        /// </summary>
        CodeBlock,
    }
}
