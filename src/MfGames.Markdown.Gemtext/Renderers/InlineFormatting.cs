namespace MfGames.Markdown.Gemtext.Renderers
{
    /// <summary>
    /// Describes the ways of formatting inline elements such as emphasis,
    /// strong, and other elements.
    /// </summary>
    public enum InlineFormatting
    {
        /// <summary>
        /// Indicates that the inline should be remove and only the text
        /// rendered.
        /// </summary>
        Remove,

        /// <summary>
        /// Indicates that the inline should be left in place in a normalized
        /// form (such as converting `_italics_` into `*italics*`).
        /// </summary>
        Normalize,
    }
}
