namespace MfGames.Markdown.Gemtext.Renderers
{
    /// <summary>
    /// Describes how links are processed within a paragraph.
    /// </summary>
    public enum BlockLinkHandling
    {
        /// <summary>
        /// Indicates that the paragraph should be broken apart and the link
        /// included on its own line in the middle of the paragraph.
        /// </summary>
        InsertLine,

        /// <summary>
        /// Indicates that all the links in a paragraph should be gathered
        /// and then emitted at the end of the paragraph. The text of the link
        /// will be left in the paragraph.
        /// </summary>
        ParagraphEnd,

        /// <summary>
        /// Indicates that all the links in the document should be gathered
        /// and then emitted at the end of the document. The text of the link
        /// will be left in the paragraph.
        /// </summary>
        DocumentEnd,

        /// <summary>
        /// Indicates that the links themselves should be removed and just the
        /// text included in the paragraph.
        /// </summary>
        Remove,
    }
}
