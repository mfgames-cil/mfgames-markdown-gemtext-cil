namespace MfGames.Markdown.Gemtext.Renderers
{
    /// <summary>
    /// Describes how a paragraph link is formatted inside the text. This is
    /// only used for `ParagraphLinkHandling.ParagraphEnd` and
    /// `ParagraphLinkHandling.DocumentEnd`.
    /// </summary>
    public enum EndLinkInlineFormatting
    {
        /// <summary>
        /// Indicates that a footnote notation (`[1]`) will be insert into the
        /// text and then the link will be displayed with the URL when gathered.
        /// </summary>
        Footnote,

        /// <summary>
        /// Indicates that the text is put in as-is into the gathered link with
        /// no footnote given in the block.
        /// </summary>
        Text,
    }
}
