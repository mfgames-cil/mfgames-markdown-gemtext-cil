using Markdig.Renderers;
using Markdig.Syntax;
using Markdig.Syntax.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext
{
    /// <summary>
    /// A base class for Gemtext rendering <see cref="Block" /> and
    /// <see cref="Inline" /> Markdown objects.
    /// </summary>
    /// <typeparam name="TObject">The type of the object.</typeparam>
    /// <seealso cref="IMarkdownObjectRenderer" />
    public abstract class GemtextObjectRenderer<TObject>
        : MarkdownObjectRenderer<GemtextRenderer, TObject>
        where TObject : MarkdownObject
    {
    }
}
