using System.Linq;
using Markdig.Syntax;
using Markdig.Syntax.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext
{
    /// <summary>
    /// Various useful extension methods for Markdig classes.
    /// </summary>
    public static class MarkdigExtensions
    {
        /// <summary>
        /// Determines if the paragraph only contains a link.
        /// </summary>
        /// <param name="obj">The object to inspect.</param>
        /// <returns>True if there is only a link in the paragraph.</returns>
        public static bool OnlyHasSingleLink(this ParagraphBlock obj)
        {
            return obj.Inline != null
                && obj.Inline.Count() == 1
                && obj.Inline.FirstChild is LinkInline;
        }

        /// <summary>
        /// Determines if the list item only contains a link.
        /// </summary>
        /// <param name="obj">The object to inspect.</param>
        /// <returns>True if there is only a link in the paragraph.</returns>
        public static bool OnlyHasSingleLink(this ListItemBlock obj)
        {
            return obj.Count == 1
                && obj.LastChild is ParagraphBlock paragraphBlock
                && paragraphBlock.OnlyHasSingleLink();
        }
    }
}
