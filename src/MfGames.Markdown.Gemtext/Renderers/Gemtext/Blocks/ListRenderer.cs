using Markdig.Syntax;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="ListBlock" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{ListBlock}" />
    public class ListRenderer : GemtextObjectRenderer<ListBlock>
    {
        protected override void Write(
            GemtextRenderer renderer,
            ListBlock listBlock)
        {
            // Lists need to be separated from the rest.
            renderer.EnsureTwoLines();

            // Go through each list item and write them out.
            foreach (var item in listBlock)
            {
                // If the list only contains a link, then we just render the
                // link instead.
                var listItem = (ListItemBlock)item;

                if (!listItem.OnlyHasSingleLink())
                {
                    renderer.EnsureLine();
                    renderer.Write("* ");
                }

                renderer.WriteChildren(listItem);
            }
        }
    }
}
