using Markdig.Syntax;
using MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="MarkdownDocument" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{ParagraphBlock}" />
    public class MarkdownDocumentRenderer
        : GemtextObjectRenderer<MarkdownDocument>
    {
        protected override void Write(
            GemtextRenderer renderer,
            MarkdownDocument obj)
        {
            // Simply write out the contents.
            renderer.WriteChildren(obj);

            // If we get to the end of the document and we have gathered links,
            // and we are in DocumentEnd mode, then write out the links. We
            // don't test for the mode here because if there are links, we
            // should write them out.
            LinkInlineRenderer.WriteGatheredLinks(renderer);
        }
    }
}
