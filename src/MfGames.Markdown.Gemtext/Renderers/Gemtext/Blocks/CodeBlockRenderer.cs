using Markdig.Syntax;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks
{
    /// <summary>
    /// An Gemtext renderer for a <see cref="CodeBlock" /> and
    /// <see cref="FencedCodeBlock" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{CodeBlock}" />
    public class CodeBlockRenderer : GemtextObjectRenderer<CodeBlock>
    {
        protected override void Write(GemtextRenderer renderer, CodeBlock obj)
        {
            // We need to have two lines above this.
            renderer.EnsureTwoLines();

            // Code blocks are always fenced, but we allow for additional text
            // at the end of them which is only in `FencedCodeBlock`.
            if (obj is FencedCodeBlock fenced)
            {
                renderer.WriteLine("```" + fenced.Info);
            }
            else
            {
                renderer.WriteLine("```");
            }

            renderer.WriteLeafRawLines(obj, true);
            renderer.Write("```");

            // If we aren't at the end of the container, then add some spacing.
            if (!renderer.IsLastInContainer)
            {
                renderer.WriteLine();
                renderer.WriteLine();
            }
        }
    }
}
