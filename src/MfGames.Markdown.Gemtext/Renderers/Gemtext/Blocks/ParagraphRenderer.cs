using Markdig.Syntax;
using MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="ParagraphBlock" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{ParagraphBlock}" />
    public class ParagraphRenderer : GemtextObjectRenderer<ParagraphBlock>
    {
        protected override void Write(
            GemtextRenderer renderer,
            ParagraphBlock obj)
        {
            // If we aren't the first in the container, we need to break apart
            // the lines to make it easier to read.
            if (!renderer.IsFirstInContainer)
            {
                renderer.EnsureTwoLines();
            }

            // We need to save the state of the link rendering while handling
            // this block.
            if (obj.OnlyHasSingleLink())
            {
                renderer.WriteLeafInline(obj);
            }
            else
            {
                renderer.WhileLinkInsideBlock(
                    () => renderer.WriteLeafInline(obj));
            }

            // If we get to the end of the paragraph and we have gathered links,
            // and we are in ParagraphEnd mode, then write out the links.
            if (renderer.BlockLinkHandling == BlockLinkHandling.ParagraphEnd)
            {
                LinkInlineRenderer.WriteGatheredLinks(renderer);
            }
        }
    }
}
