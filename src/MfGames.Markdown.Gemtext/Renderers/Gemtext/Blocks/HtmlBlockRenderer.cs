using Markdig.Syntax;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="GemtextBlock" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{GemtextBlock}" />
    public class HtmlBlockRenderer : GemtextObjectRenderer<HtmlBlock>
    {
        protected override void Write(GemtextRenderer renderer, HtmlBlock obj)
        {
            // If we are stripping out HTML blocks (default), then nothing to
            // do with rendering.
            if (renderer.HtmlBlockFormatting == HtmlBlockFormatting.Remove)
            {
                return;
            }

            // Otherwise, we treat this as a fenced code block.
            renderer.EnsureTwoLines();
            renderer.WriteLine("```html");
            renderer.WriteLeafRawLines(obj, true);
            renderer.WriteLine("```");

            // If we aren't at the end of the container, then add some spacing.
            if (!renderer.IsLastInContainer)
            {
                renderer.WriteLine();
            }
        }
    }
}
