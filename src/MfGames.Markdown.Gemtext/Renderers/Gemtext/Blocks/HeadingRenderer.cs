using Markdig.Syntax;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks
{
    /// <summary>
    /// An Gemtext renderer for a <see cref="HeadingBlock" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{HeadingBlock}" />
    public class HeadingRenderer : GemtextObjectRenderer<HeadingBlock>
    {
        private int currentHeading;

        /// <summary>
        /// Gets or sets a value indicating whether the header depths are
        /// increased after the first one.
        /// </summary>
        public bool IncreaseHeaderDepthAfterFirst { get; set; }

        protected override void Write(
            GemtextRenderer renderer,
            HeadingBlock obj)
        {
            // Figure out the level we should be processing.
            int level = obj.Level;

            if (this.currentHeading++ > 0 && this.IncreaseHeaderDepthAfterFirst)
            {
                // Check the second header we see. If this header is H2 or
                // higher, then we assume that the file has been already updated
                // to handle the heading and we stop processing.
                if (this.currentHeading == 2 && level != 1)
                {
                    this.IncreaseHeaderDepthAfterFirst = false;
                }
                else
                {
                    // We are bumping the heading levels up.
                    level++;
                }
            }

            // Write out the prefix of the header.
            string prefix = level switch
            {
                1 => "# ",
                2 => "## ",
                3 => "### ",
                _ => "",
            };

            renderer.EnsureTwoLines();
            renderer.Write(prefix);
            renderer.WriteLeafInline(obj);
        }
    }
}
