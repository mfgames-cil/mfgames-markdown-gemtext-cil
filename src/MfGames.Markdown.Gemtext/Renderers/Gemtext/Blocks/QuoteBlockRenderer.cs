using Markdig.Syntax;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="QuoteBlock" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{QuoteBlock}" />
    public class QuoteBlockRenderer : GemtextObjectRenderer<QuoteBlock>
    {
        protected override void Write(GemtextRenderer renderer, QuoteBlock obj)
        {
            string quoteIndent = obj.QuoteChar + " ";

            renderer.EnsureTwoLines();
            renderer.PushIndent(quoteIndent);
            renderer.WriteChildren(obj);
            renderer.PopIndent();
        }
    }
}
