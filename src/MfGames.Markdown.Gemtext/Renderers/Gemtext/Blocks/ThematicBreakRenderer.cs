using Markdig.Syntax;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Blocks
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="ThematicBreakBlock" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{ThematicBreakBlock}" />
    public class ThematicBreakRenderer
        : GemtextObjectRenderer<ThematicBreakBlock>
    {
        protected override void Write(
            GemtextRenderer renderer,
            ThematicBreakBlock obj)
        {
            renderer.EnsureTwoLines();
            renderer.WriteLine("---");
        }
    }
}
