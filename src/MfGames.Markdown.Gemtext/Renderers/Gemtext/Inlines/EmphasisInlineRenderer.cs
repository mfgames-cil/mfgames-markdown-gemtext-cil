using Markdig.Syntax.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines
{
    /// <summary>
    /// A Gemtext renderer for an <see cref="EmphasisInline" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{EmphasisInline}" />
    public class EmphasisInlineRenderer : GemtextObjectRenderer<EmphasisInline>
    {
        protected override void Write(
            GemtextRenderer renderer,
            EmphasisInline obj)
        {
            InlineFormatting formatting = renderer.EmphasisFormattingResolved;
            bool normalize = formatting == InlineFormatting.Normalize;
            string delimiter = new string('*', obj.DelimiterCount);

            if (normalize)
            {
                renderer.Write(delimiter);
            }

            renderer.WriteChildren(obj);

            if (normalize)
            {
                renderer.Write(delimiter);
            }
        }
    }
}
