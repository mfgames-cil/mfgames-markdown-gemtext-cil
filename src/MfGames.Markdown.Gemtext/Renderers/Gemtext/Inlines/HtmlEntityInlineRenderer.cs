using Markdig.Syntax.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="GemtextEntityInline" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{GemtextEntityInline}" />
    public class HtmlEntityInlineRenderer
        : GemtextObjectRenderer<HtmlEntityInline>
    {
        protected override void Write(
            GemtextRenderer renderer,
            HtmlEntityInline obj)
        {
            renderer.Write(obj.Transcoded);
        }
    }
}
