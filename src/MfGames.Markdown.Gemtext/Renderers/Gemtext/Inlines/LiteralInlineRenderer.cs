using Markdig.Syntax.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="LiteralInline" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{LiteralInline}" />
    public class LiteralInlineRenderer : GemtextObjectRenderer<LiteralInline>
    {
        protected override void Write(
            GemtextRenderer renderer,
            LiteralInline obj)
        {
            // If we are inside a paragraph and we are doing inline formatting,
            // then we need to trim the text if we are before or after a link.
            string content = obj.Content.ToString();
            BlockLinkHandling handling = renderer.BlockLinkHandling;
            bool isInsert = handling == BlockLinkHandling.InsertLine;
            bool inBlock = renderer.LinkInsideBlock;

            if (inBlock && isInsert)
            {
                if (obj.PreviousSibling is LinkInline)
                {
                    content = content.TrimStart();
                }

                if (obj.NextSibling is LinkInline)
                {
                    content = content.TrimEnd();
                }
            }

            // Write out the manipulated content.
            renderer.Write(content);
        }
    }
}
