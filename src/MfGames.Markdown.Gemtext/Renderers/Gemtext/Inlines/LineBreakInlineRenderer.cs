using Markdig.Syntax.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="LineBreakInline" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{LineBreakInline}" />
    public class LineBreakInlineRenderer
        : GemtextObjectRenderer<LineBreakInline>
    {
        /// <summary>
        /// Gets or sets a value indicating whether to render this softline break as a
        /// Gemtext hardline break tag (&lt;br /&gt;)
        /// </summary>
        public bool RenderAsHardlineBreak { get; set; }

        protected override void Write(
            GemtextRenderer renderer,
            LineBreakInline obj)
        {
            if (obj.IsHard || this.RenderAsHardlineBreak)
            {
                renderer.EnsureTwoLines();
            }
            else
            {
                renderer.Write(" ");
            }
        }
    }
}
