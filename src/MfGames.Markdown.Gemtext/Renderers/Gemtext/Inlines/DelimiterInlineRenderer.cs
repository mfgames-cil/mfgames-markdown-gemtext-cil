using Markdig.Syntax.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="DelimiterInline" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{DelimiterInline}" />
    public class DelimiterInlineRenderer
        : GemtextObjectRenderer<DelimiterInline>
    {
        protected override void Write(
            GemtextRenderer renderer,
            DelimiterInline obj)
        {
            renderer.Write(obj.ToLiteral());
            renderer.WriteChildren(obj);
        }
    }
}
