using Markdig.Syntax.Inlines;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="CodeInline" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{CodeInline}" />
    public class CodeInlineRenderer : GemtextObjectRenderer<CodeInline>
    {
        protected override void Write(GemtextRenderer renderer, CodeInline obj)
        {
            const string Delimiter = "`";
            InlineFormatting formatting = renderer.CodeFormattingResolved;
            bool normalize = formatting == InlineFormatting.Normalize;

            if (normalize)
            {
                renderer.Write(Delimiter);
            }

            renderer.Write(obj.Content);

            if (normalize)
            {
                renderer.Write(Delimiter);
            }
        }
    }
}
