using System;
using System.Net;
using Markdig.Extensions.SmartyPants;

namespace MfGames.Markdown.Gemtext.Renderers.Gemtext.Inlines
{
    /// <summary>
    /// A Gemtext renderer for a <see cref="SmartyPant" />.
    /// </summary>
    /// <seealso cref="GemtextObjectRenderer{GemtextEntityInline}" />
    public class GemtextSmartyPantRenderer
        : GemtextObjectRenderer<SmartyPant>
    {
        private static readonly SmartyPantOptions DefaultOptions = new();

        private readonly SmartyPantOptions options;

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlSmartyPantRenderer" /> class.
        /// </summary>
        /// <param name="options">The options.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public GemtextSmartyPantRenderer(SmartyPantOptions? options)
        {
            this.options = options
                ?? throw new ArgumentNullException(nameof(options));
        }

        protected override void Write(GemtextRenderer renderer, SmartyPant obj)
        {
            if (!this.options.Mapping.TryGetValue(obj.Type, out string? text))
            {
                DefaultOptions.Mapping.TryGetValue(obj.Type, out text);
            }

            string? unicode = WebUtility.HtmlDecode(text);

            renderer.Write(unicode);
        }
    }
}
